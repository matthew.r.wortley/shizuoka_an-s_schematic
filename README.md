# Shizuoka_AN-S_Schematic

Use at your own risk. My Shizuoka AN-S has been converted to linuxcnc. These are the Kicad drawings of how I wired it. They are for information only. Use of any of the information contained in them is done at your own risk. You have been warned.

## Description

These schematics are the electrical drawings for my Shizuoka AN-S CNC Milling Machine. They will evolve over time. To edit them, you will need Kicad 6.xx

Open the file shiz-mill_safety_and_power.kicad_pro with Kicad to open the project.

PDF's of the schematics and the panel layout are provided for convenience.

The schematics make use of my Industrial electrical symbols library for Kicad. https://gitlab.com/matthew.r.wortley/ki-cad-symbols-mrw

The layout makes use of my Industrial_Misc footprints library for Kicad. https://gitlab.com/matthew.r.wortley/ki-cad-footprints-mrw

Both are forks of the kicad 6.xx libraries. Maybe I can convince the Kicad librarians to have some industrial symbols for those of that need them.

It is a bit of a peculiar use of Kicad. A few things are worth noting: 

* Wires have a \~*nn designation. \~* This keeps from using wire numbers again. Just like any component (i.e. U5) they need to annotate uniquely, and can be auto-annotated, which can be convenient. 
* Contacts from a contactor or industrial relay have a "unit suffix" of A, B, C etc. This is just to show they are part of the same unit -- Ignore the suffix when reading the schematics. 
* Every Terminal has a suffix to show which one in the strip it is. i.e -X1.7A is on terminal strip X1, 7th terminal over, A (top) position. This prevents landing more than 1 wire on a terminal, which is important with the cage clamp terminals I used.